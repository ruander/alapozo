<?php

//Termék osztály

class Product
{
    /*private float $price;
    private string $name;*/

    //konstruktor, akkor fut amikor a new parancs
    /**
     * @param string $name
     * @param float $price
     */
    public function __construct(
        private string $name,
        private float $price)
    {
        //$this->name = $name . ' - teszt';
        //$this->price = $price;
    }

    //lehessen a termék árához áfát adni

    /**
     * @param float $rate
     * @return $this Product
     */
    public function addTax(float $rate): Product
    {
        $this->price += $this->price * $rate / 100;
        //láncolhatóság
        return $this;
    }

    //lehessen árleszállítást csinálni egy terméken
    public function addDiscount(float $rate): Product
    {
        $this->price -= $this->price * $rate / 100;
        //láncolhatóság
        return $this;
    }


    //addDiscount($rate)
    //ár visszaadása, mert private
    public function getPrice(): float
    {
        return $this->price;
    }
    //név visszaadása
    //getName()
    public function getName(): string
    {
        return $this->name;
    }

    //termék kiírása
    public function display(): string
    {
        $output = '<article class="product">';
        $output .= "<h2>{$this->name}</h2>";
        $output .= "<div><b>Ára:</b> {$this->price}.-HUF</div>";
        $output .= '</article>';
        return $output;
    }
    //objectum echo esetén adja vissza a display()-t
    public function __toString(): string
    {
        return $this->display();
    }

    public function __destruct()
    {
        echo 'Fut a destructor';
    }
}
