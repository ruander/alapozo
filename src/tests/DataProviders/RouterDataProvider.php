<?php

namespace Tests\DataProviders;

class RouterDataProvider
{
    public function routeNotFoundCases(): array
    {
        return [
            ['/users','put'],//method not found
            ['invoices','post'],//not found, found
            ['/users','get'],//class not found
            ['/users','post'],//method doesnt exists
        ];
    }
}
