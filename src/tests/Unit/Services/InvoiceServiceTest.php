<?php
declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Services\EmailService;
use App\Services\InvoiceService;
use App\Services\PaymentGatewayService;
use App\Services\SalesTaxService;
use PHPUnit\Framework\TestCase;

class InvoiceServiceTest extends TestCase
{
    /** @test */
    public function it_process_invoice_(): void
    {
        $salesTaxServiceMock = $this->createMock(SalesTaxService::class);
        $gatewayServiceMock = $this->createMock(PaymentGatewayService::class);
        $emailServiceMock = $this->createMock(EmailService::class);

        /*        var_dump($salesTaxServiceMock->calculate(100,[]));//alapesetben nullal tér vissza minden mockon hívott funkció, ha meg van adva tipus akkor azzal function ...(): type
                exit;*/

        //Stubbing
        $gatewayServiceMock->method('charge')->willReturn(true);

        //adott szolgáltatás (InvoiceService)
        $invoiceService = new InvoiceService(
            $salesTaxServiceMock,
            $gatewayServiceMock,
            $emailServiceMock
        );
        $customer = ['name' => 'John'];
        $amount = 1500;

        //amikor meghívjuk a process-t
        $result = $invoiceService->process($customer, $amount);


        //Akkor a szolgáltatás sikeresen lefut
        $this->assertTrue($result);
    }

    /** @test */
    public function it_sends_email_when_invoice_processed(): void
    {
        $salesTaxServiceMock = $this->createMock(SalesTaxService::class);
        $gatewayServiceMock = $this->createMock(PaymentGatewayService::class);
        $emailServiceMock = $this->createMock(EmailService::class);


        //Stubbing
        $gatewayServiceMock->method('charge')->willReturn(true);

        $emailServiceMock
            ->expects($this->once())
            ->method('send')
            ->with(['name' => 'John'],'receipt');

        //adott szolgáltatás (InvoiceService)
        $invoiceService = new InvoiceService(
            $salesTaxServiceMock,
            $gatewayServiceMock,
            $emailServiceMock
        );
        $customer = ['name' => 'John'];
        $amount = 1500;

        //amikor meghívjuk a process-t
        $result = $invoiceService->process($customer, $amount);


        //Akkor a szolgáltatás sikeresen lefut
        $this->assertTrue($result);
    }
}
