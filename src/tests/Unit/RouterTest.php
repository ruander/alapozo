<?php
declare(strict_types=1);

namespace Tests\Unit;

use App\Exceptions\RouteNotFoundException;
use App\Router;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase
{
    private Router $router;

    public function setUp():void
    {
        parent::setUp();

        $this->router = new Router();
    }

    /** @test */
    public function it_registers_a_route(): void
    {
        //given-when-then
        //arrange-act-assert
        //megadunk egy routert objektumot
//        $router = new Router();

        //amikor meghívjuk a register() metódust
        $this->router->register('get', '/users', ['Users', 'index']);

        //akkor ellenőrizzük hogy a route regisztrálva lett
        $expected = [
            'get' => [
                '/users' => ['Users', 'index']
            ]

        ];
        $this->assertSame($expected, $this->router->routes());
    }

    /** @test */
    public function it_registers_a_get_route(): void
    {
        $this->router->get('/users', ['Users', 'index']);

        $expected = [
            'get' => [
                '/users' => ['Users', 'index']
            ]

        ];
        $this->assertSame($expected, $this->router->routes());
    }

    /** @test */
    public function it_registers_a_post_route(): void
    {
        $this->router->post('/users', ['Users', 'store']);

        $expected = [
            'post' => [
                '/users' => ['Users', 'store']
            ]

        ];
        $this->assertSame($expected, $this->router->routes());
    }
    /** @test  */
    public function there_are_no_routes_when_router_created(): void
    {
        $router = new Router();

        $this->assertEmpty($router->routes());
    }


    /**
     * @test
     * @dataProvider \Tests\DataProviders\RouterDataProvider::routeNotFoundCases
     */
    public function it_throws_rout_not_found_exception(
        string $requestUri,
        string $requestMethod
    ): void
    {
        $users = new class() {
            public function delete(): bool
            {
                return true;
            }

        };
        $this->router->post('/users',[$users::class,'store']);
        $this->router->get('/users',['Users','index']);

        $this->expectException(RouteNotFoundException::class);
        $this->router->resolve($requestUri, $requestMethod);
    }

    /** @test */
    public function it_resolves_a_route_from_closure(): void
    {
        $this->router->get('/users', fn() => [1,2,3] );

        $this->assertSame(
            [1,2,3],
            $this->router->resolve('/users','get')
        );
    }

    /** @test */
    public function it_resolves_a_route(): void
    {
        $users = new class() {
            public function index(): array
            {
                return [1,2,3];
            }

        };

        $this->router->get('/users', [$users::class,'index']);

        $this->assertSame( //assertSame == | assertSame ===
            [1,2,3],
            $this->router->resolve('/users','get')
        );
    }
}
