<?php

namespace App;

trait LatteTrait
{
    private string $typeOfMilk = "Teljes tej";
    public static string $a = 'property teszt...'. PHP_EOL;

    public function makeLatte()
    {
        echo static::class . '| Latte készítése [' . $this->typeOfMilk . ']' . PHP_EOL;
    }

    public static function test()
    {
        echo 'Ez egy teszt...' . PHP_EOL;
    }

    public function setTypeOfMilk(string $typeOfMilk): static
    {
        $this->typeOfMilk = $typeOfMilk;

        return $this;
    }
}
