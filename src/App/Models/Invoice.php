<?php

namespace App\Models;

use App\Model;

class Invoice extends Model
{

    public function create(float $amount, int $userId): int
    {
        $stmt = $this->db->prepare(
            'INSERT INTO invoices(user_id,amount)
                            VALUES( ? , ? )'
        );

        $stmt->execute([$userId, $amount]);

        return (int)$this->db->lastInsertId();
    }

    //createMany(...)
    //1 elem lekérése azonisító alapján
    public function find(int $invoiceId): array
    {
        $stmt = $this->db->prepare(
            "SELECT invoices.id as invoice_id, amount, user_id, name
            FROM invoices
            LEFT JOIN users ON user_id = users.id
            WHERE invoices.id = ?");

        $stmt->execute([$invoiceId]);

        $invoice = $stmt->fetch();

        return $invoice ? $invoice : [];
    }
}
