<?php

namespace App\Models;

use App\Model;

class SignUp extends Model
{

    /**
     * @param User $userModel
     * @param Invoice $invoiceModel
     */
    public function __construct(protected User $userModel, protected Invoice $invoiceModel)
    {
        parent::__construct();
    }

    public function register(array $userInfo, array $invoiceInfo): int
    {
        try {
            $this->db->beginTransaction();//tranzakció indítása
            //User Model terv

            $userId = $this->userModel->create($userInfo['email'],$userInfo['name'],true);//a beillesztett userId val vagy false tér vissza az adatátadás szebben megvalósítható dto (data transfer object megoldással)

            $invoiceId = $this->invoiceModel->create($invoiceInfo['amount'],$userId);//a beillesztett invoiceId val vagy false tér vissza

            $this->db->commit();
        } catch (\Throwable $e) {
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }
            throw $e;
        }

        return $invoiceId;
    }


}
