<?php

namespace App\Models;

use App\Model;

class User extends Model
{

    public function create(string $email,string $name, bool $isActive = false): int
    {
        $stmt = $this->db->prepare(
            'INSERT INTO users(email,name,is_active,time_created) 
                            VALUES(?,?,?, NOW())'
        );

        $stmt->execute([$email,$name,$isActive]);

        return (int) $this->db->lastInsertId();
    }

    //createMany(...)
    //queryBuilder minta (doctrine) https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/query-builder.html
}
