<?php

namespace App;

class CollectionAgency implements DebtCollector
{

    public function collect(float $owedAmount): float
    {
        $minAmount = $owedAmount * .5;

        return mt_rand($minAmount,$owedAmount);
    }


}
