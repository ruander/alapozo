<?php

namespace App;

class classA
{
    protected static string $name = 'A';

    public static function getName(): string
    {
        return static::$name;
    }

    public static function test(): static
    {
        return new static();
        //return static::getName();
    }
}
