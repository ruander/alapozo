<?php

namespace App;

use App\Exceptions\ViewNotFoundException;

class View
{

    /**
     * @param string $string
     */
    public function __construct( protected string $view , protected array $params = [])
    {
    }

    public static function make(string $view, array $params = []): static
    {
        return new static($view, $params);
    }

    public function render(): string
    {
        $viewPath = VIEW_PATH . $this->view . '.php';
        //ha nincs ilyen file, exeption
        if(!file_exists($viewPath)){
            throw new ViewNotFoundException();
        }
        /*foreach ($this->params as $key => $value){
            //variable variables
            $$key = $value;
        }*/

        //vagy

        extract($this->params);

        //nézet 'megjelenítése'
        ob_start();
        include $viewPath;

        return (string) ob_get_clean();

    }

    public function __toString(): string
    {
        //var_dump('fut a tostring)');
        return $this->render();
    }

    public function __get(string $name)
    {
        return $this->params[$name] ?? null;
    }
}
