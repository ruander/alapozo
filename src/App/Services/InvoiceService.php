<?php

declare(strict_types = 1);

namespace App\Services;

class InvoiceService
{

    public function __construct(
        protected SalesTaxService $salesTaxService,
        protected PaymentGatewayService $gatewayService,
        protected EmailService $emailService
    )
    {
    }

    public function process(array $customer, float $amount): bool
    {

        // 1. adó
        $tax = $this->salesTaxService->calculate($amount, $customer);

        // 2. számla
        if (! $this->gatewayService->charge($customer, $amount, $tax)) {
            return false;
        }
        //$customer['email'] = 'test@example.com';
        // 3. email küldése
        $this->emailService->send($customer, 'receipt');

        return true;
    }
}
