<?php

namespace App\Exception;

class MissingBillingInfoException extends \Exception
{
    protected $message = 'Üres számlázási adatok!';

}
