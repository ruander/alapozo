<?php
declare(strict_types=1);

namespace App\Controllers;

use App\Models\Invoice;
use App\Models\SignUp;
use App\Models\User;
use App\View;
use Faker;

class HomeController
{
    public function index(): View
    {
        $faker = Faker\Factory::create('hu_HU');

        $name = $faker->name();
        $email = $faker->email();

        $amount = 4990;

        $userModel = new User();
        $invoiceModel = new Invoice();
        //SignUp folyamat minta
        $invoiceId = (new SignUp($userModel,$invoiceModel))->register(
            [
                'email' => $email,
                'name' => $name
            ],
            [
                'amount' => $amount
            ]
        );

        //kérjük le a bevitt adatokat (teszt...)

        return View::make('index', ['title' => 'Home Page', 'invoice'=> $invoiceModel->find($invoiceId)]);

    }

    public function download()
    {
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment;filename="testfile.pdf"');

        readfile(STORAGE_PATH . '/a-digitalis-szamitogep-mukodesi-elve.pdf');
    }

    public function upload()
    {
        /*echo '<pre>';
        var_dump($_FILES);
        echo '</pre>';*/
        $filePath = STORAGE_PATH . $_FILES['fileToUpload']['name'];
        move_uploaded_file(
            $_FILES['fileToUpload']['tmp_name'],
            $filePath
        );

        header('Location: /');
        exit;
        /* echo '<pre>';
         var_dump(pathinfo($filePath));
         echo '</pre>';*/
        unlink(STORAGE_PATH . '/a-digitalis-szamitogep-mukodesi-elve.pdf');
    }

    public function phpinfo()
    {
        \phpinfo();
    }
}
