<?php

namespace App;

/**
 * @property-read string $a
 * @property-write float|int $b
 * @property int $c
 */
class Transaction
{
    public array $data = [];#itt lesznek a már definiált propertyk
    /**
     * @var array|string[]
     */
    protected array $validPropertyTypes = [
        'a' => 'string',
        'b' => 'double|integer',
        'c' => 'integer'
    ];
    /**
     * @var array|array[]
     */
    protected array $validProperties = [
        'a' => [
            'types' => ['string'],
            'mode' => 'readonly'
        ],
        'b' => [
            'types' => ['double','integer'],
            'mode' => 'writeonly'
        ],
        'c' => [
            'types' => ['integer'],
            'mode' => null
        ]
    ];

    /**
     * @todo HF az órán bemutatott és megoldott getterek és setterek újból megvalósítása, de most a $validPropertTypes vagy (és ez a jobb) $validProperties tömbök felhasználásával
     *
     */
   /* public function __get(string $name)
    {
        if (!in_array($name, ['a', 'c'])) {
            echo "You are trying to reach an invalid property [$name]" . PHP_EOL;
            return '';
        }
        if ($name === 'a' && !array_key_exists($name, $this->data)) {
            $this->data[$name] = $this->generateRandomString();
        }
        if ($name === 'c' && !array_key_exists($name, $this->data)) {
            $this->data[$name] = null;
        }

        return $this->data[$name];
    }

    public function __set(string $name, $value): void
    {
        if (!in_array($name, ['b', 'c'])) {
            echo "You are trying to set an invalid property [$name]" . PHP_EOL;
        } else {
            switch ($name) {
                case 'b':
                    if (gettype($value) !== 'double') {
                        trigger_error(" b értéke csak lebegőpontos lehet", E_USER_NOTICE);
                    } else {
                        $this->data[$name] = $value;
                    }
                    break;
                case 'c':
                    if (gettype($value) !== 'integer') {
                        trigger_error(" c értéke csak egész szám lehet", E_USER_NOTICE);
                    } else {
                        $this->data[$name] = $value;
                    }
                    break;
            }

        }
    }*/

    protected function generateRandomString(int $length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
