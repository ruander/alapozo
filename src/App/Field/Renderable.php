<?php

namespace App\Field;

interface Renderable
{
    public function render(): string;
}
