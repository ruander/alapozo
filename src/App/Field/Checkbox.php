<?php

namespace App\Field;

class Checkbox extends Radio
{
    public function render(): string
    {
        return '<input type="checkbox" name="'.$this->name.'">';
    }
}
