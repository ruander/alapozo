<?php

namespace App\Field;

class Email extends Text
{
    public function render(): string
    {//megvalósítás - 'hogyan'
        return '<input type="email" name="'.$this->name.'" class="'.$this->class.'">';
    }
}
