<?php

namespace App\Field;

class Textarea extends Field
{

    public function render(): string
    {
        return '<textarea name="'.$this->name.'" class="'.$this->class.'"></textarea>';
    }
}
