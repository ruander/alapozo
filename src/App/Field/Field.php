<?php

namespace App\Field;

abstract class Field implements Renderable
{
    public function __construct(
        protected string $name,
        protected string|array|null $class = null )
    {
        $this->class = 'form-input '. (is_array($class) ? implode(' ', $class) : $class);
    }

}

/** @todo Órai feladat
+textarea
+email (text)
 *
+kiegészítés hogy legyen a mezőknek egy olyan classa hogy 'form-input', de egy nem kötelező tulajdonságként lehessen hozzáfűzni string vagy stringből álló tömbből 1 vagy több osztályt <input type="text" name="name" class="form-input ...string vagy tömb elemei"
 * ['one','two',three'] -> class="form-input one two three"
 * 'one' class="form-input one"
 *
 */
