<?php

namespace App\Field;

class Text extends Field
{

    public function render(): string
    {//megvalósítás - 'hogyan'
        return '<input type="text" name="'.$this->name.'" class="'.$this->class.'">';
    }
}
