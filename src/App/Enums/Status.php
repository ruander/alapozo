<?php

namespace App\Enums;

class Status
{
//fizetési státuszok
    public const PENDING = 'pending';
    public const PAID = 'paid';
    public const DECLINED = 'declined';

    public const ALL_STATUSES = [
        self::PENDING => 'Folyamatban',
        self::PAID => 'Fizetve',
        self::DECLINED => 'Elutasítva'
    ];
}
