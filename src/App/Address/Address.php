<?php
/** @todo: postal_code legyen védett, és ellenőrzés után vegyük csak át (városnév alapján kereshet viceversa) */
namespace App\Address;

class Address
{
    /** @todo !!! */
    //Osztály állandók felvétele (residence [1], Shipping [2], Temporary [3] )
    // ADDRESS_TYPE_RESIDENCE = 1
    const ADDRESS_TYPE_RESIDENCE = 1;
    const ADDRESS_TYPE_SHIPPING = 2;
    const ADDRESS_TYPE_TEMPORARY = 3;


    //statikus tömb (elérhető) ahol a kulcs a tipus [n] érték a kiírandó tipus
    public static array $valid_address_types = [
        self::ADDRESS_TYPE_RESIDENCE => 'Állandó',
        self::ADDRESS_TYPE_SHIPPING => 'Szállítási',
        self::ADDRESS_TYPE_TEMPORARY => 'Ideiglenes'
    ];
    //szükséges változók:
    // address_line_1,address_line_2,city_name,country_name,postal_code
    public string $address_line_1;
    public string $address_line_2;
    public string $city_name;
    public string $country_name;
    public string $postal_code;

    // védett változók legyenek:
    // address_type_id -> egyenlőre elérhető és állítható (1,2,3 !!!DE csak létező id lehessen) az objektumban (később lesz védett)
    // _time_created -> Konstruktorban legyen felvéve az aktuális timestampre
    protected int $_address_type_id;

    protected int $_time_created;

    public function __construct(array $data = [])
    {
        $this->_time_created = \time();

        if (!is_array($data)) {
            trigger_error("Nem lehet létrehozni objektumot a kapott adatokból az Address osztályból");
        } else {
            foreach ($data as $name => $value) {
                if (
                    in_array($name, ['time_created', 'address_type_id'])
                ) {
                    $name = '_' . $name;
                }
                $this->$name = $value;
            }

        }

    }

    public function __get(string $name)
    {
        echo "<br>Fut a get ($name)";
        //ha szükség van védett tulajdonság visszaadására, és az létezik
        $protected_property_name = '_' . $name; //_valami_nev_ami_letezik_de_vedett
        if (property_exists($this, $protected_property_name)) {//ha létezik adjuk vissza
            return $this->$protected_property_name;
        }
        //ha nincs ilyen, hiba
        trigger_error("Nem létező tulajdonságot próbáltunk elérni");

        return false ;
    }

    public function __set(string $name, $value): void
    {
        //echo "<br>Fut a set ($name , $value)";
        //csak létező address type idt lehessen átvenni
        if ($name === 'address_type_id') {
            $this->_setAddressTypeId($value);//id beállítás ha érvényes
        }
    }

    //display()
    public function display(): string
    {
        $output = '<div class="card" style="width: 18rem;">';
        $output .= '<div class="card-body">
                            <h5 class="card-title">'.self::$valid_address_types[$this->_address_type_id].' cím</h5>';
        $output .= $this->address_line_1;
        if($this->address_line_2){//ha van cim 2. sor
            $output .= '<br>'.$this->address_line_2;
        }
        $output .= '<br>'.$this->postal_code. ','. $this->city_name;
        $output .= '<br><b>'.$this->country_name.'</b>';
        $output .= '</div></div>';

        return $output;
    }
    //__toString
    public function __toString(): string
    {
        return $this->display();
    }

    /**
     * Címtipus azonosító ellenőrzése
     * @param int $address_type_id
     * @return bool
     */
    public static function isValidAddressTypeId(int $address_type_id): bool
    {
        return array_key_exists($address_type_id, self::$valid_address_types);
    }

    /**
     * @param int $address_type_id
     */
    protected function _setAddressTypeId(int $address_type_id): void
    {
        if (self::isValidAddressTypeId($address_type_id)) {
            $this->_address_type_id = $address_type_id;
        }
    }
}
