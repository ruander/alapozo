<?php

namespace App\PaymentGateway\Otp;


class Transaction
{
    private float $amount;

    public function __construct($amount)
    {
            $this->amount = $amount;
    }
/*
    //getter - kiolvasni
    public function getAmount(): float
    {
        return $this->amount;
    }

    //setter - beállításra
    public function setAmount( float $amount): void
    {
        $this->amount = $amount;
    }*/

    public function process()
    {
        echo "<br> {$this->amount}.- HUF összegű tranzakció feldolgozása";

        //számla készítése
        $this->generateBill();

        //email küldése
        $this->sendEmail();

    }

    private function generateBill()
    {
        return true;
    }

    private function sendEmail()
    {
        return true;
    }
}


