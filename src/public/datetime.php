<?php

require_once __DIR__ .'/../vendor/autoload.php';

$datetime = new DateTime('12/1/2021 16:00');

//var_dump($datetime);
echo $datetime->getTimezone()->getName(). ' | '.$datetime->format('m/d/Y g:i A').PHP_EOL;

$datetime->setTimezone(new DateTimeZone('Europe/Budapest'));
$datetime->setDate(2021,7,20)->setTime(20,30);
//var_dump($datetime);

echo $datetime->getTimezone()->getName(). ' | '.$datetime->format('m/d/Y g:i A').PHP_EOL;

#probléma módosított dátummal
//$from = new DateTime();
//$to = (new DateTime())->add(new DateInterval('P5D'));
//$to = (clone $from)->add(new DateInterval('P5D'));

$from = new DateTimeImmutable();
$to = $from->add(new DateInterval('P5D'));

echo $from->format('m/d/Y') . ' - ' . $to->format('m/d/Y').PHP_EOL;

var_dump($from,$to);

/**
 * @todo HF: kösd be a Carbont és teszteld le miket tud (public/carbon-test.php)
 * @link https://github.com/briannesbitt/Carbon
 */
