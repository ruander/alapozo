<?php
require __DIR__ . "/../vendor/autoload.php";

use App\Field\{Text, Checkbox, Radio, Textarea, Email};

$inputs = [
    //new \App\Field('baseField'),
    new Text('textField',[
        'testclass1',
        'testclass1'
    ]),
    //new \App\Boolean('boolField'),
    new Radio('radioField'),
    new Checkbox('checkboxField'),
    new Textarea('textarea','one'),
    new Email('emailField',[
        'testclass1',
        'testclass1'
    ]),
    new Textarea('textarea2'),

];

foreach ($inputs as $field){
    echo $field->render().'<br>';
}
