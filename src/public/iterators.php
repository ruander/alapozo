<?php

use App\InvoiceCollection;

require_once __DIR__ . '/../vendor/autoload.php';

/*$datePeriod = new DatePeriod(
    new DateTime('01/01/2022'),
    new DateInterval('P1D'),
    new DateTime('02/01/2022')
);

foreach ($datePeriod as $date) {
    echo $date->format('Y-m-d'). PHP_EOL;
}*/

$invoiceCollection = new InvoiceCollection([
    new \App\Invoice(950),
    new \App\Invoice(2400),
    new \App\Invoice(19990)
]);

foreach ($invoiceCollection as $invoice) {
    //var_dump($invoice);
    echo $invoice->id . ' : ' . $invoice->amount . PHP_EOL;
}

test($invoiceCollection);
test([1,2,3]);
//test('a');

function test(iterable $iterable){

    foreach ($iterable as $item){
        //...
        var_dump($item);
    }
}
