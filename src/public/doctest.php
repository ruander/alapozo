<?php
require_once __DIR__ .'/../vendor/autoload.php';
#egysoros komment
//egysoros komment
/*
 * blokk komment
 */
/**
 * Docblock
 *
 * @tag
 * @
 */

$transaction = new \App\Transaction();
echo $transaction->a . PHP_EOL;#warning mert nem létezik '...még :)'
$transaction->a = 50;#docblock jelzés
echo $transaction->b;
$transaction->b = 12.5;
$transaction->b = '12.5';
echo $transaction->c;
$test_b = $transaction->b;#docblock jelzés
$transaction->c = 'hello';
$test_c = $transaction->c;
$transaction->d = 'test';#nem létező property (nincs docblockban se)
echo $transaction->a . PHP_EOL;
echo $transaction->unexisting . PHP_EOL;
$transaction->c = 150;
var_dump($transaction);
/**
 * @todo [KÉSZ!] Transaction osztály kiegészítése hogy megfelelő tipusu a b és c elemekkel lehessen csak feltölteni a többit hagyja figyelmen kivül vagy tájékoztasson a nem létező elem írásról-olvasásáról
 * a csak -read $a elem a property első(!) kiolvasásakor keletkezzen az objektumban (bármilyen generált string jó)
 * konstruktor nélkül!!!
 *
 * @todo 2 : típusok ellenőrzése (ha nem megfelelő tipusu adatot próbálunk megadni, ne vegye fel ,hanem dobjon hibát). a folyamatot egészítsd ki trigger_error notice tipusu üzenetekkel
 */
