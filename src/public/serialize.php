<?php
require_once __DIR__ . '/../vendor/autoload.php';

use App\Invoice;

$invoice = new Invoice(1000, 'Some invoice',123456789);

//echo serialize(true) . PHP_EOL,
//serialize(1). PHP_EOL,
//serialize(3.14). PHP_EOL,
//serialize('valamiő'). PHP_EOL,
//serialize([1, 2, 3]). PHP_EOL,
//serialize([
//    'a' => 1,
//    'b' => 2
//]).PHP_EOL;

//var_dump(unserialize(serialize(['a'=>1,'b'=>2])));
$mySerializedObject = serialize($invoice);
echo $mySerializedObject . PHP_EOL;
//

$invoice2 = unserialize($mySerializedObject);

var_dump($invoice2);
