<?php
/** @todo órai minifeladatok:
 * Metódus végrehajtási sorrend tesztelése:
 Baseclass,childClass,trait

 * statikus elem a traitből:
 lehet-e statikus elemet használni/felülírni?

 * abstract metódus érkezhet-e traitből

 */
require_once __DIR__ .'/../vendor/autoload.php';

//$coffeeMachine = new \App\CoffeeMachine();
//$coffeeMachine->makeEspresso();
//
//$latteMachine = new \App\LatteMachine();
//$latteMachine->makeEspresso();
//$latteMachine->setTypeOfMilk('Gőzölt tej');
//$latteMachine->makeLatte();
//
//$cappuccinoMachine = new \App\CappuccinoMachine();
//$cappuccinoMachine->makeEspresso();
//$cappuccinoMachine->makeCappuccino();
//
//
//$allInOneCoffeeMachine = new \App\AllInOneCoffeeMachine();
//$allInOneCoffeeMachine->makeEspresso();
//$allInOneCoffeeMachine->setTypeOfMilk('Fincsi tejszínes tej');
//$allInOneCoffeeMachine->makeLatte();
//$allInOneCoffeeMachine->makeCappuccino();

//\App\LatteMachine::test();
\App\LatteMachine::$a = 'TEST A';
\App\AllInOneCoffeeMachine::$a = 'TEST B';

echo \App\LatteMachine::$a . ' | ' . \App\AllInOneCoffeeMachine::$a. PHP_EOL;
/** @todo: HF az öröklésben a statikus elemek az objektumok szempontjából globális -> Teszteld le, hogy TRAIT esetében hogyan működik
 - 14. alkalom kezdés: static property bemutatása
 -(hint) használható a Traitben a __CLASS__
 */
