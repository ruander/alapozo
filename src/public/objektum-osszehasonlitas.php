<?php
require_once __DIR__ . '/../vendor/autoload.php';

use App\CustomInvoice;
use App\Invoice;

$invoice1 = new Invoice(new \App\Customer('Vásárló 1'),100,'Számla');
$invoice2 = new Invoice(new \App\Customer('Vásárló 1'),100,'Számla');

/*elakad mert végtelen csatolást okoz
$invoice1->attachedInvoice = $invoice2;
$invoice2->attachedInvoice = $invoice1;
*/
echo '$invoice1 == $invoice2'. PHP_EOL;
var_dump($invoice1 == $invoice2);

echo '$invoice1 === $invoice2'. PHP_EOL;
var_dump($invoice1 === $invoice2);

var_dump($invoice1,$invoice2);
