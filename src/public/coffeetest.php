<?php
require_once __DIR__ .'/../vendor/autoload.php';

$coffeeMachine = new \App\CoffeeMachine();
$coffeeMachine->makeEspresso();

$latteMachine = new \App\LatteMachine();
$latteMachine->makeEspresso();
$latteMachine->makeLatte();

$cappuccinoMachine = new \App\CappuccinoMachine();
$cappuccinoMachine->makeEspresso();
$cappuccinoMachine->makeCappuccino();

$allInOneCoffeeMachine = new \App\AllInOneCoffeeMachine();
$allInOneCoffeeMachine->makeEspresso();
$allInOneCoffeeMachine->makeLatte();
$allInOneCoffeeMachine->makeCappuccino();

