<?php
//declare(strict_types=1);
echo '<h2>Switch és Match</h2>';
/** @todo Órai feladat: examResult (0-5)
 * Írd ki a vizsgaeredményt, és
 * 0-1: Sikertelen vizsga
 * 2-4: Sikeres vizsga
 * 5: sikeres vizsga dícsérettel
 * minden egyéb: ismeretlen vizsgaeredmény!
 */
$examResult = rand(1, 10);
echo '<br>';
echo "Vizsgaeredmény: <b>$examResult</b>";
echo '<br>';
switch ($examResult) {
    case 0:
    case 1:
        echo 'Sikertelen vizsga!';
        break;
    case '2':
    case '3':
    case '4':
        echo 'Sikeres vizsga!';
        $a=5;
        echo $a;
        break;
    case 5:
        echo "Sikeres vizsga dícsérettel!";
        break;
    default:
        echo "Ismeretlen vizsgaeredmény!";
        break;
}

//match
$examResultDisplay = match ($examResult) {
    0,1 =>  'Sikertelen vizsga',
    2,3,4 =>  'Sikeres vizsga!',
    5 =>  'Sikeres vizsga dícsérettel!',
    default =>  'Ismeretlen vizsgaeredmény!'
};

echo $examResultDisplay;
