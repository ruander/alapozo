<?php
require __DIR__ . "/../../vendor/autoload.php";

use App\PaymentGateway\Otp\Transaction;
//Encapsulation - Zártság elve
//Abstraction - Absztrakció
//Inheritance - Öröklés
//Polymorphism - Többalakúság

$transaction = new Transaction(1500);

$transaction->process();

