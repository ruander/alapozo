<?php
declare(strict_types=1);
//osztály betöltése
require_once '../../Product.php';

echo '<pre>';
//objektum létrehozása
$product = new Product('Termék 1', 1600);
var_dump($product);
//die();

/*$product->name = 'Termék 1 uj';
$product->price = 2000.00;
var_dump($product->name);*/

echo 'Termék ára:'.$product->getPrice();
$product2 = new Product('Termék 2', 1990.00);
//adjunk áfát a termékhez
$product2->addTax(27);
$product2->addDiscount(50);
var_dump($product2);

//display eljárás segítségével kiírjuk a termékeket
echo $product->display();
echo $product2->display();

echo $product;
unset($product);
$product2 = null;
$product3 = new Product('Termék 3', 10000);
//26%áfa, 10% kedvezmény
;
var_dump($product3);
echo $product3->addTax(27)
    ->addDiscount(10)
    ->display();

//egy lépésben
//nem javasolt változó osztálynév
$className = 'Product';
$product4= (new $className('Termék 4',25000))
    ->addTax(27)
    ->addDiscount(10);
echo $product4;
