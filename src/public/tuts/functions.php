<?php
/** @todo Órai feladat: 5 db függvény + demo hívások (2-3 db / funkció) */
//2 paraméterrel
//Összeadás

//kivonás

//szorzás

//osztás

//hatványozás


declare(strict_types=1);

function name(): mixed
{//string|int|float|array|bool   void mixed  ?int -> nullable  <= union types |||
    return rand(1, 100) % 2 === 0 ? 50 : 'nonono';
}

echo name();
var_dump(name());
//szorzás
function multiply(int|float $a, int|float $b = 10): int|float
{
    return $a * $b;
}

echo '<br>';
$result = multiply(3, 5);
var_dump($result);

function multiplyDouble(int|float &$a, int|float $b): int|float
{//referencia átadása
    $a *= 2;
    return $a * $b;
}

$a = 3;
$b = 4;
echo '<pre>';
echo multiplyDouble($a, $b);
var_dump($a, $b);

//... operátor
function multiplyAnyAmountOfNumbers(int|float ...$numbers): int|float
{
    $ret = 1;
    foreach ($numbers as $number) {
        $ret *= $number;
    }
    return $ret;
}

$numbers = [5, 6.00, 7, 8];
echo multiplyAnyAmountOfNumbers(...$numbers);
var_dump($numbers);
var_dump(...$numbers);

//osztás
/**
 * @param int|float $a
 * @param int|float $b
 * @return int|float|bool
 */
function divide(int|float $a, int|float $b): int|float|bool
{
    if($b === 0){
        return false;
    }elseif($a % $b === 0){
        return fdiv($a,$b);
    }
    return $a;
}

$a = 8;
$b = 4;

echo divide(b: $b,a: $a);
echo divide($a, b:$b);

//asszociatív vagy nem asszociativ tömbből -> argumentum
$array = [
    8,
    'b' => 4
];
echo divide(...$array);
//setcookie('test1','value1',0,'','',false,true);
//setcookie('test2','value2',httponly: true);

//scope
test();
function test(){
    global $a;
    $a = 10;
    echo ++$GLOBALS['a'];
}

var_dump($a);

//változónév eljárások

$a = 9;
$b = 3;

$fv = 'divide';
if(is_callable($fv)){
    echo $fv($a,$b);
}else{
    echo 'Nincs ilyen eljárás: '. $fv;
}
