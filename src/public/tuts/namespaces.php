<?php
/**
 * Név hatókör: @link:https://www.php.net/manual/en/language.namespaces.rules.php
 * PSR standardek @link:https://www.php-fig.org/psr
 */
/*require_once "../../App/PaymentGateway/PayPal/Transaction.php";
require_once "../../App/PaymentGateway/Otp/Transaction.php";
require_once "../../App/PaymentGateway/Otp/CustomerProfile.php";
require_once "../../App/Notification/Email.php";*/
//require_once "../../App/PaymentGateway/Otp/DateTime.php";

spl_autoload_register( function ($classname){
    $classPath = __DIR__.'/../../'.str_replace('\\','/',$classname).'.php';
    //var_dump($classPath);
    if(is_file($classPath)){//Autoloader implementations MUST NOT throw exceptions, MUST NOT raise errors of any level, and SHOULD NOT return a value.
        require $classPath;
    }
});


use App\PaymentGateway\PayPal\Transaction as PaypalTransaction;
//use PaymentGateway\Otp\{Transaction as OtpTransaction,CustomerProfile};
use App\PaymentGateway\Otp;


$paypalTransaction = new PaypalTransaction();
var_dump($paypalTransaction);

$otpTransaction = new Otp\Transaction();
var_dump($otpTransaction);

$otpCustomerProfile = new Otp\CustomerProfile();
var_dump($otpCustomerProfile);
/*$transaction = new Transaction();
var_dump($transaction);//a use miatt a paypalből származik*/
